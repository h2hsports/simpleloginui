﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Amazon.Lambda;
using Amazon.Runtime;
using Amazon.CognitoIdentity;
using Amazon;
using System.Text;
using Amazon.Lambda.Model;

public class LoginButtonScript : MonoBehaviour {
	private string identityPoolID = "us-east-1:731531d4-35d8-4d2d-9534-589d218fc01b";
    //private CognitoAWSCredentials cognitoCredentials;
    private BasicAWSCredentials awsCredentials;
	private IAmazonLambda _lambdaClient;

	private IAmazonLambda Client
	{
		get
		{
			if (_lambdaClient == null)
			{
				_lambdaClient = new AmazonLambdaClient(awsCredentials, RegionEndpoint.USEast1);
			}
			return _lambdaClient;
		}
	}

	// Use this for initialization
	void Start () {
        /*
		cognitoCredentials = new CognitoAWSCredentials (
			identityPoolID, // Identity Pool ID
			RegionEndpoint.USEast1 // Region
			);	
            */
        awsCredentials = new BasicAWSCredentials("AKIAIW26W7LSVQ3ID5JA", "D6kBNmn0iRM9xgoCE5TGdHH4IVmLjC9+69X4+UgV");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnButton() {
		Debug.Log ("In On Button!");

		GameObject emailAddressGo = GameObject.Find ("UserNameInputField");
		InputField emailAddressInputField = emailAddressGo.GetComponent<InputField> ();
		GameObject passwordGo = GameObject.Find ("PasswordInputField");
		InputField passwordInputField = passwordGo.GetComponent<InputField> ();

		Debug.Log ("Username is: " + emailAddressInputField.text);
		Debug.Log ("Password is: " + passwordInputField.text);

		string payloadString = "{\"userName\":\"" + emailAddressInputField.text + "\",\"passwordHash\": \"" + passwordInputField.text + "\" }";
		Debug.Log ("payload: " + payloadString);
		string payloadResponse = "";
		Debug.Log ("Invoking AuthenticateUser function in Lambda... \n");
		Client.InvokeAsync(new Amazon.Lambda.Model.InvokeRequest()
		                   {
			FunctionName = "arn:aws:lambda:us-east-1:099848894266:function:AuthenticateUser",
			Payload = payloadString
		},
		(responseObject) =>
		{
			if (responseObject.Exception == null)
			{
				payloadResponse += Encoding.ASCII.GetString(responseObject.Response.Payload.ToArray()) + "\n";
			}
			else
			{
				payloadResponse += responseObject.Exception + "\n";
			}
		}
		);

		Debug.Log ("function response: " + payloadResponse);

	}
}
